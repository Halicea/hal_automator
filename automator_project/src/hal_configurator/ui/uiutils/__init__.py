def layout_widgets(layout):
  return (layout.itemAt(i) for i in range(layout.count()))